const express = require("express");
const app = express();
const cors = require("cors");
const pool = require("./db");


//middleware
app.use(cors());
app.use(express.json());


//ROUTES


//create new employee

app.post("/createemployee", async (req, res) => {
  try {
    console.log(req.body)
    const { name, employeeid, role, number } = req.body;
    let employee = req.body;
    const newemployee = await pool.query(
      "INSERT INTO employeelist (name,employeeid,role,number) VALUES($1,$2,$3,$4) RETURNING *",
      [name, employeeid, role, number]
    );
    res.json(newemployee.rows[0]);
  } catch (err) {
    console.error(err.message);
  }

});

//get all employee

app.get("/allemployee", async (req, res) => {
  try {
    const allemployee = await pool.query("SELECT * FROM employeelist");
    res.json(allemployee.rows);
  } catch (err) {
    console.error(err.message);
  }
});

//delete a employee

app.delete("/employee/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const deleteemployee = await pool.query("DELETE FROM employeeList WHERE id = $1", [
      id
    ]);
    res.json("employee deleted!");
  } catch (err) {
    console.log(err.message);
  }
});

//update employee data

app.put("/employee/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const { name, employeeid, role, number } = req.body;
    const updateemployee = await pool.query(
      "UPDATE employeeList SET name = $1 ,employeeid =$2 ,role =$3 ,number =$4 WHERE id = $5",
      [name, employeeid, role, number, id]
    );
    res.json("employee was updated");
  } catch (err) {
    console.error(err.message);
  }
});


app.listen(5000, () => {
  console.log("server started at port 5000")
})