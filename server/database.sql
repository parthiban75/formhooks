CREATE DATABASE employeedata;

CREATE TABLE employeeList(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    employeeid VARCHAR(255),
    role VARCHAR(255),
    number VARCHAR(255)

);